FROM python:3.6

RUN pip install --upgrade pip

RUN mkdir -p /root/django
RUN adduser --disabled-password --gecos '' alex
USER alex
WORKDIR /root/django

RUN pip install django==1.8

ENV PATH="/root/django.local/bin:${PATH}"

COPY --chown=alex:alex . /root/django

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

